package co.com.mockup.mockup.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import co.com.mockup.mockup.R;

public class LogginActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    public static final String USER_TYPE = "userType";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loggin_activity);

        ImageView imageView = findViewById(R.id.logo);
        imageView.setImageResource(R.drawable.logo);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        Button button = findViewById(R.id.confirmationButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInformation();
            }
        });
    }

    private void validateInformation(){

        if (username.getText().toString().equals(password.getText().toString())){
            SharedPreferences.Editor preferences = getSharedPreferences(USER_TYPE, Context.MODE_PRIVATE).edit();
            switch (username.getText().toString()){
                case "Admin":
                    preferences.putString(USER_TYPE, "Administrador");
                    break;
                case "Client":
                    preferences.putString(USER_TYPE, "Cliente");
                    break;
                case "Owner":
                    preferences.putString(USER_TYPE, "Propietario");
                    break;
                default:
                    logginError();
                    break;
            }
            preferences.apply();
            finish();
        }else{
            logginError();
        }
    }

    private void logginError(){
        Toast toast = Toast.makeText(getApplicationContext(), "Usuario y contraseña incorrectos", Toast.LENGTH_LONG);
        toast.show();
    }
}
