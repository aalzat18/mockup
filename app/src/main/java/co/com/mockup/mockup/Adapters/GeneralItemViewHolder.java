package co.com.mockup.mockup.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import co.com.mockup.mockup.R;

public class GeneralItemViewHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView description;
    public ImageView image;

    public GeneralItemViewHolder(View itemView) {
        super(itemView);

        title = itemView.findViewById(R.id.item_title);
        description = itemView.findViewById(R.id.item_description);
        image = itemView.findViewById(R.id.item_image);
    }
}
