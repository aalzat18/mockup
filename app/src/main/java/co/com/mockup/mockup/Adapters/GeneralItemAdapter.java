package co.com.mockup.mockup.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import co.com.mockup.mockup.Models.ListItemModel;
import co.com.mockup.mockup.R;

public class GeneralItemAdapter extends RecyclerView.Adapter<GeneralItemViewHolder> {

    private Context context;
    private List<ListItemModel> items;

    public GeneralItemAdapter(Context context, List<ListItemModel> items){

        this.context = context;
        this.items = items;
    }

    @NonNull
    @Override
    public GeneralItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemsView = LayoutInflater.from(context).inflate(R.layout.general_item, parent, false);
        GeneralItemViewHolder item = new GeneralItemViewHolder(itemsView);

        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull GeneralItemViewHolder holder, int position) {

        holder.title.setText(items.get(position).getTitle());
        holder.description.setText(items.get(position).getDescription());
        holder.image.setImageResource(items.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
