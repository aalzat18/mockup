package co.com.mockup.mockup.Factories;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import co.com.mockup.mockup.Models.ListItemModel;
import co.com.mockup.mockup.R;

public class ProfileFactory {

    private List<ListItemModel> items;
    public static final String USER_TYPE = "userType";

    public List<ListItemModel> profilePage(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences(USER_TYPE, Context.MODE_PRIVATE);
        String userType = preferences.getString(USER_TYPE, null);

        switch (userType){
            case "Administrador":
                adminProfile();
                break;
            case "Cliente":
                clientProfile();
                break;
            case "Propietario":
                ownerProfile();
                break;
        }

        return items;
    }

    private void adminProfile(){

        items = new ArrayList<>();
        items.add(new ListItemModel("Cargar Evento", "Aqui puedes crear eventos para que los demás usuarios los vean", R.drawable.ic_profile_menu));
        items.add(new ListItemModel("Eliminar Evento", "Elimina los eventos que ya no tienen vigencia", R.drawable.ic_profile_menu));
        items.add(new ListItemModel("Contabilidad", "Revisa la parte contable y administrativa de tu negocio", R.drawable.ic_profile_menu));
    }

    private void clientProfile(){

        items = new ArrayList<>();
        items.add(new ListItemModel("Registra tus facturas", "Entre más facturas registres, más puntos obtendrás", R.drawable.ic_profile_menu));
        items.add(new ListItemModel("Quejas y reclamos", "Presenta tus problemas frente al administrador", R.drawable.ic_profile_menu));
        items.add(new ListItemModel("Politicas de privacidad", "Mira los terminos y condiciones de la aplicación", R.drawable.ic_profile_menu));
    }

    private void ownerProfile(){

        items = new ArrayList<>();
        items.add(new ListItemModel("Pago de facturas", "Mira tus facturas a vencer y obten beneficios por pronto pago", R.drawable.ic_profile_menu));
        items.add(new ListItemModel("Ver pagos", "Mira los pagos realizados en los meses anteriores", R.drawable.ic_profile_menu));
        items.add(new ListItemModel("Quejas y reclamos", "Presenta tus problemas frente al administrador", R.drawable.ic_profile_menu));
    }
}
