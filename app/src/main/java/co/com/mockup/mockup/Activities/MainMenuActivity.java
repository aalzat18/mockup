package co.com.mockup.mockup.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import co.com.mockup.mockup.Fragments.HomeMenuFragment;
import co.com.mockup.mockup.Fragments.MessageMenuFragment;
import co.com.mockup.mockup.Fragments.Profile.AdminProfileFragment;
import co.com.mockup.mockup.Fragments.Profile.ClientProfileFragment;
import co.com.mockup.mockup.Fragments.ProfileMenuFragment;
import co.com.mockup.mockup.R;

public class MainMenuActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_activity);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new HomeMenuFragment());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()){
            case R.id.navigation_home:
                fragment = new HomeMenuFragment();
                break;
            case R.id.navigation_mail:
                fragment = new MessageMenuFragment();
                break;
            case R.id.navigation_profile:
                fragment = new AdminProfileFragment();
                break;
        }

        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment){

        if (fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();

            return true;
        }
        return false;
    }
}
