package co.com.mockup.mockup.Fragments.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.com.mockup.mockup.Activities.LogginActivity;
import co.com.mockup.mockup.Adapters.GeneralItemAdapter;
import co.com.mockup.mockup.Factories.ProfileFactory;
import co.com.mockup.mockup.Models.ListItemModel;
import co.com.mockup.mockup.R;

public class AdminProfileFragment extends Fragment {

    private TextView title;
    private ImageView image;
    private Button button;
    private RecyclerView recyclerView;
    private GeneralItemAdapter adapter;
    private List<ListItemModel> items;
    private View view;

    public static final String USER_TYPE = "userType";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.admin_profile_fragment, container, false);
        setupView();

        button = view.findViewById(R.id.closeButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LogginActivity.class);
                getActivity().startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupView();
    }

    private void setupView(){
        SharedPreferences preferences = getActivity().getSharedPreferences(USER_TYPE, Context.MODE_PRIVATE);
        String user = preferences.getString(USER_TYPE, null);

        ProfileFactory factory = new ProfileFactory();

        items = factory.profilePage(getActivity());

        adapter = new GeneralItemAdapter(getContext(), items);


        title = view.findViewById(R.id.sectionTitle);
        title.setText(user);

        image = view.findViewById(R.id.imageView);
        image.setBackgroundResource(R.drawable.logo_santafe);

        recyclerView = view.findViewById(R.id.recyclerViewList);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }
}
